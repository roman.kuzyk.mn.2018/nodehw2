const express = require('express');
const mongoose = require('mongoose');
const app = express();
const fs = require('fs');
const path = require('path');
const authRoute = require('./routes/auth');
const postsRoute = require('./routes/posts');
const profileRoute = require('./routes/profile');
const morgan = require('morgan');
app.use(express.json());
app.use('/api/auth', authRoute);
app.use('/api/users', profileRoute);
app.use('/api', postsRoute);
// const env = require('dotenv');
const dbURI = 'mongodb+srv://admin1:ganja4ever@nodecrud.4qzno.mongodb.net/CRUD?retryWrites=true&w=majority';
mongoose.connect(dbURI, {useNewUrlParser: true, useUnifiedTopology: true})
    .then((result) => console.log('connected to db'))
    .catch((err) => console.log('Error - ', err));
// const db = mongoose.connection;
const debug = require('debug')('hw1:server');
const http = require('http');
const PORT = process.env.PORT;
console.log(PORT);

// Logs
const accessLogStream = fs
    .createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'});
app.use(morgan('combined', {stream: accessLogStream}));

app.use(morgan(':method :url :status :res[content-length]- :response-time ms'));


const port = '8080';
app.set('port', port);
console.log(port);

const server = http.createServer(app);
server.listen(port);

server.on('error', onError);
server.on('listening', onListening);
/**
 *
 * @param {*} error
 *
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ?
      'Pipe ' + port :
      'Port ' + port;


  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 *
 */
function onListening() {
  console.log(port);

  const addr = server.address();
  const bind = typeof addr === 'string' ?
      'pipe ' + addr :
      'port ' + addr.port;
  debug('Listening on ' + bind);
}


module.exports = app;
