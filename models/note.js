const mongoose = require('mongoose');
const model = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },

});

module.exports = mongoose.model('Note', model);
