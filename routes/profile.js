const express = require('express');
const router = new express.Router();

const middleware = require('../middlewares');
const verifyToken = middleware.verify;
const User = require('../models/user');
router.get('/me', verifyToken, (req, res, next) => {
  const userId = req.user.data._id;
  User.findOne({_id: userId})
      .then((note) => {
        console.log('post ', note);
        if (note) {
          const user = {
            _id: userId,
            username: note.username,
            createdDate: note.createdDate,
          };
          res.status(200).json({user});
          console.log('post', user);
          next();
        } else {
          res.status(400).json({message: 'User not found'});
        }
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});


router.delete('/me', verifyToken, (req, res, next) => {
  const userId = req.user.data._id;
  User.findOne({_id: userId}).then((note) => {
    if (note) {
      User.remove({_id: userId}).then(() => {
        res.status(200).json({message: `${userId} deleted successfully`});
        next();
      });
    } else {
      res.status(400).json({message: 'Invalid user id'});
    }
  });
});

module.exports = router;
