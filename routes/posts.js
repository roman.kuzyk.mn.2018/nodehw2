const express = require('express');
const router = new express.Router();
const Note = require('../models/note');
const middleware = require('../middlewares');
const verifyToken = middleware.verify;


router.get('/notes', verifyToken, (req, res, next) => {
  let limit = +req.query.limit;
  let offset = +req.query.offset;
  let count;
  if (!offset) offset = 0;
  if (!limit) limit = 0;
  const userId = req.user.data._id;
  Note.find({userId: userId}).skip(offset).limit(limit)
      .then((notes) => {
        if (notes) {
          count = notes.length;
          res.status(200).json({limit, offset, count, notes});
          console.log('post', notes);
          next();
        }
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});

router.post('/notes', verifyToken, (req, res, next) => {
  const userId = req.user.data._id;
  console.log('req.user', req.user.data._id);
  console.log('userId ', userId);
  console.log('req.body.text', req.body.text);
  const newPost = new Note({userId: userId,
    text: req.body.text, completed: false});
  newPost.save()
      .then(() => {
        res.status(200).json({
          message: 'success',
          text: req.body.text,

        });
        next();
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});

router.get('/notes/:id', verifyToken, (req, res, next) => {
  const test = req.params.id;
  Note.findOne({_id: test})
      .then((note) => {
        if (note) {
          res.status(200).json({note});
          console.log('post', note);
          next();
        } else {
          res.status(400).json({message: 'Wrong post id'});
        }
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});
router.patch('/notes/:id', verifyToken, (req, res, next) => {
  const postId = req.params.id;
  Note.findOne({_id: postId})
      .then((note) => {
        if (note) {
          note.completed = !note.completed;
          Note.updateOne({_id: postId}, {
            $set: {
              completed: note.completed,
            },
          })
              .then(() => {
                res.status(200).json({message: 'success', note});
                next();
              });
        } else {
          res.status(400).json({message: 'Wrong post id'});
        }
      })
      .catch((error) => {
        res.status(500).json(error);
      });
});

router.delete('/notes/:id', verifyToken, (req, res, next) => {
  const postId = req.params.id;
  Note.findOne({_id: postId}).then((note) => {
    if (note) {
      Note.remove({_id: postId}).then(() => {
        res.status(200).json({message: `${postId} deleted successfully`});
        next();
      });
    } else {
      res.status(400).json({message: 'Invalid post id'});
    }
  });
});

module.exports = router;
