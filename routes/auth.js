const express = require('express');
const router = new express.Router();
const User = require('../models/user');
const bcrypt = require('bcrypt');
const rounds = 10;

const jwt = require('jsonwebtoken');
const tokenSecret = 'my-token-secret';

const middleware = require('../middlewares');

router.post('/login', (req, res) => {
  if (req.body.username && req.body.password) {
    User.findOne({username: req.body.username})
        .then((user) => {
          if (!user) {
            res.status(400)
                .json({message: 'no user with that username found'});
          } else {
            bcrypt.compare(req.body.password, user.password, (error, match) => {
              console.log(match);

              if (error) res.status(500).json(error);
              else if (match) {
                res.status(200).json({message: 'succes',
                  jwt_token: generateToken(user)});
              } else res.status(400).json({message: 'passwords do not match'});
            });
          }
        })
        .catch((error) => {
          res.status(500).json(error);
        });
  } else {
    res.status(400).json({message: 'Invalid request body'});
  }
});

router.post('/register', (req, res) => {
  if (req.body.username && req.body.password) {
    User.findOne({username: req.body.username})
        .then((user) => {
          if (user) {
            res.status(400)
                .json({message: 'User with that username is already exists'});
          } else {
            bcrypt.hash(req.body.password, rounds, (error, hash) => {
              if (error) res.status(500).json(error);
              else {
                const newUser = new User({username: req.body.username,
                  password: hash});
                newUser.save()
                    .then((user) => {
                      res.status(200).json({
                        message: 'succes',
                        jwt_token: generateToken(user)});
                    })
                    .catch((error) => {
                      res.status(500).json(error);
                    });
              }
            });
          }
        });
  } else {
    res.status(400).json({message: 'Invalid request body'});
  }
});

router.get('/jwt-test', middleware.verify, (req, res) => {
  res.status(200).json(req.user);
});
/**
 *
 * @param {*} user
 * @return {string}
 */
function generateToken(user) {
  return jwt.sign({data: user}, tokenSecret, {expiresIn: '24h'});
}

module.exports = router;
