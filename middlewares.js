const jwt = require('jsonwebtoken');
const tokenSecret = 'my-token-secret';

exports.verify = (req, res, next) => {
  const token = req.headers.authorization;
  console.log(token);
  console.log(token.split(' ')[1]);

  if (!token) res.status(400).json({message: 'please provide a token'});
  else {
    jwt.verify(token.split(' ')[1], tokenSecret, (err, user) => {
      if (err) res.status(500).json({message: 'failed to authenticate token'});
      req.user = user;
      next();
    });
  }
};
